<?php
/*
Template Name: About Page Template
*/
?>

<?php
get_header();
?>




  <main class="main">
    <section class="aboutSection">
      <div class="container">
        <h2 class="aboutSection__title section__title">
          о<span class="redText"> нас </span>
        </h2>
        <div class="aboutSection__inner">
          <div class="aboutSection__block">
            <div class="aboutSection__text">
              <p>
                Наш брендовый магазин Premium Outlet предлагает широкий ассортимент аксессуаров на любой вкус.
                Мы не гонимся за единовременной прибылью, мы дорожим репутацией. Для нас важно, чтобы покупатель остался доволен и стал постоянным. Вот почему мы предлагаем товар только высочайшего качества по приемлемым ценам и без предоплаты. Перед отправкой каждое изделие проходит контроль качества. Каждый аксессуар приходит в фирменной упаковке. Всем модницам,даже из самых удаленных уголков России, мы предлагаем доставку по всей территории нашей страны.
              </p>
              <p>
                Остались вопросы? Позвоните по телефону <a href="tel:+79207670990"><strong>+7 (920) 767-09-90</strong></a> или напишите нам в Whatssap,менеджеры ответят на интересующие вас вопросы и помогут сделать заказ!
              </p>
            </div>
            <a href="https://wa.me/+79207670990" class="social__link">
              <div class="social__text">НАПИСАТЬ НА WHATSAPP</div>
              <img class="social__img" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/logo_whatsapp.svg" alt="" >
            </a>
          </div>
          <div class="aboutSection__block">
            <div class="aboutSection__text">
              <p>
                Перед отправкой каждое изделие проходит контроль качества. Каждый аксессуар приходит в фирменной упаковке. Всем модницам,даже из самых удаленных уголков России, мы предлагаем доставку по всей территории нашей страны.
              </p>
            </div>
            <div class="aboutSection__imgBlock">
              <div class="aboutSection__imgWrapper">
                <img class="aboutSection__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/aboutBanner.png" alt="" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

<?php
// get_sidebar();
get_footer();
