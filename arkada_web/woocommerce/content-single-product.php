<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<section class="productCardSection">
	<div class="container">

	<!-- breadcrumbs start -->
	<?php
	/**
	 * Hook: woocommerce_before_single_product.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 */
	do_action( 'woocommerce_before_single_product' );
	// breadcrumbs end
	?>

    <h2 class="productCardSection__title section__title"><?php echo $product->get_title(); ?> </h2>
    <div class="productCard">
      <div class="productCard__imagesBlock">
        <!-- <img class="productCard__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" > -->
        <?php
        // echo $product->get_image(); ?>


        <?php
          $product = wc_get_product(get_the_ID());
          $img = $product->get_image('large'); ?>
          <div class="row">
            <div class="col-12">
              <div class="single_item_main_pic">
                <?=$img;?>
              </div>
            </div>
            <div class="col-12">
              <?php
                $attachment_ids = $product->get_gallery_image_ids();
                  if ($attachment_ids) {
                    echo '<div class="single_item_pic">';
                    foreach ( $attachment_ids as $attachment_id ) {
                      $full_src          = wp_get_attachment_image_src( $attachment_id, 'large' );
                      $small_src          = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );?>
                      <a class="single_item_gallery" data-value="<?=$full_src[0];?>" rel="group" href="<?=$full_src[0];?>"><img src="<?=$small_src[0];?>" alt="" /></a>
                      <?php
                    }
                    echo '</div>';
                  }
              ?>
            </div>
          </div>


        <?php
        //  $imgIds = $product->get_gallery_image_ids(); 
        // var_dump($imgIds); 
        //     var_dump("test"); 
            ?>
      </div>
      <div class="productCard__textContent">
        <div class="productCard__heading productCard__brand">БРЕНД: <span class="redText">PINKO</span></div>
        <div class="productCard__desc">
          <div class="productCard__heading">ОПИСАНИЕ:</div>
          <div class="productCard__text">
            <?php $imgIds = $product->get_description(); ?>
          </div>
        </div>
        <div class="productCard__digitals">
          <div class="productCard__quantity productCardQuantity">
            <div class="productCard__heading">Количество:</div>
            <div class="productCardQuantity__block">


              <div class="cartBlockProduct__item cartBlockProduct__item--quantity">
                <div class="order__item-column order__item-column--count productCardQuantity__block">
                  <button class="productCardQuantity__itemWrapper productCardQuantity__itemWrapper--minus" data-num="-1">-</button>
                    <input class="productCardQuantity__itemWrapper productCardQuantity__itemWrapper--value" type="number" value='1' min="1" max="99" readonly>
                  <button class="productCardQuantity__itemWrapper productCardQuantity__itemWrapper--plus" data-num="+1">+</button>
                </div>
              </div>


              <!-- <div class="productCardQuantity__itemWrapper">-</div>
              <div class="productCardQuantity__itemWrapper">1</div>
              <div class="productCardQuantity__itemWrapper">+</div> -->
            </div>
          </div>
          <div class="productCard__price">
            <div class="productCard__heading">стоимость: <span class="redText"><?php echo $product->get_price_html(); ?> </span></div>
          </div>
        </div>
        <div class="productCard__buttons">
        <!-- <div class="product__subtitle"> -->

          <div class="customBtnToCartWrapper">
            <a href="?add-to-cart=<?= $product->get_ID(); ?>&quantity=<?=$_productQty;?>" class="product__btn add_to_cart_button ajax_add_to_cart" data-product_id="<?= $product->get_ID(); ?>" data-quantity="<?=$_productQty;?>" data-product_sku="" aria-label="Добавить &quot;<?= $product->get_title(); ?>&quot; в корзину" rel="nofollow">
            В КОРЗИНУ
          </a>
          </div>


          <div class="product__likeImgWrapper">
            <a href="?add_to_wishlist=<?= $product->get_ID(); ?>" class="add_to_wishlist single_add_to_wishlist addToWL addToWL--productPage" data-product-id="<?= $product->get_ID(); ?>" data-product-type="simple" data-original-product-id="<?= $product->get_ID(); ?>" data-title="Добавить в список желаний" rel="nofollow">
              <span>В избранное</span>
              <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              <!-- <span>test</span> -->
            </a>
            <a class="addedToWL d_none_custom" href="/wishlist/">В избранное</a>
          </div>

          <!-- <button class="productCard__button product__btn">В корзину</button> -->
          <!-- <button class="productCard__button product__btn product__btn--like">В избранное</button> -->
        </div>
      </div>
    </div>
	</div>
</section>
<section class="reviews">
  <div class="container">
    <h3 class="reviews__title">Отзывы</h3>
    <div class="reviews__sliderWrapper">
      <div class="reviews__slider reviewsSlider">
      <?php
        $query = new WP_Query(array(
          'post_type'=> 'otzivi',
          'posts_per_page'=>-1,
          'order'    => 'ASC'
        ));

        while ($query->have_posts()) {
          $query->the_post();
          ?> 
          <div class="reviewsSlider__slide reviewsSlide">
              <div class="reviewsSlide__text">
              <?php the_field('otzyv_soderzhimoe'); ?>
              </div>
              <div class="reviewsSlide__author"><?php the_field('otzyv_avtor'); ?></div>
              <div class="reviewsSlide__date"><?php the_field('otzyv_data'); ?></div>
            </div>
          <?php
        }

        wp_reset_query();
      ?>
      </div>
    </div>
  </div>
</section>


<section class="bestsellers featured">
  <div class="container">
    <div class="promo__titleWrapper">
      <h2 class="promo__title">РЕКОМЕНДУЕМЫЕ ТОВАРЫ</h2>
    </div>
    <div class="bestsellers__slider bestsellersSlider">
          <?php $hits = get_field("hity", 27);?>
          <?php 
          foreach ($hits as $hit) {
            $product = wc_get_product($hit);
            //$shop_url = get_permalink(wc_get_page_id('shop'));
            $product_id = $product->get_ID();
            $product_sku = $product->get_SKU();
            $image_id  = $product->get_image_id();
            $img = wp_get_attachment_image_url( $image_id, 'full' );
            $title = $product->get_title();
            $desc = $product->get_short_description();
            $fulldesc = $product->get_description();
            ?>
            <div class="bestsellersSlider__item">
            <div class="product">
              <a href="<?= $product->get_permalink();?>" class="product__link">
                <div class="product__imgwrapper">
                  <img class="product__img" src="<?= $img?>" alt="" >
                </div>
              </a>
              <div class="product__title">
                <div class="product__name"><?= $title?></div>
                <div class="product__price">
                  <?php if($product) : ?>
                      <?php if($product->sale_price): ?>
                          <div class="price-old ">
                          <span>
                              <strong><?= $product->regular_price; ?></strong>
                              <span >p.</span>
                          </span>
                      </div>
                      <div class="price-current price-sale">
                          <strong> <?= $product->sale_price; ?></strong>
                          <span >p.</span>
                      </div>
                      <?php elseif ($product->regular_price): ?>
                      <div class="price-current">
                          <?= $product->regular_price; ?>
                          <span >p.</span>
                      </div>
                      <? else :?>
                      <div class="price-current">
                          <strong> Цена по запросу</strong>
                      </div>
                      <?php endif; ?>
                  <?php endif; ?>	
                </div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <a href="?add_to_wishlist=<?= $product_id; ?>" class="add_to_wishlist single_add_to_wishlist addToWL" data-product-id="<?= $product_id; ?>" data-product-type="simple" data-original-product-id="<?= $product_id; ?>" data-title="Добавить в список желаний" rel="nofollow">
                    <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    <!-- <span>test</span> -->
                  </a>
                  <a class="addedToWL d_none_custom" href="/wishlist/">В избранное</a>
                </div>
                <div class="customBtnToCartWrapper">
                  <a href="?add-to-cart=<?= $product_id; ?>" class="product__btn add_to_cart_button ajax_add_to_cart" data-product_id="<?= $product_id; ?>" data-product_sku="" aria-label="Добавить &quot;<?= $title; ?>&quot; в корзину" rel="nofollow">
                  В КОРЗИНУ
                </a>
                </div>
              </div>
            </div>
          </div>
          <?php
          }
          // $shortcode = '[products ids="' . implode(",", $hits) . '"]';
          // echo do_shortcode($shortcode);
          ?>
          

        </div>
  </div>
</section>

<!-- ниже идет футер -->
<?php 
do_action( 'woocommerce_after_single_product' ); 
?>
