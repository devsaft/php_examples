<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */


defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
?>



<main class="main">
	<section class="shopSection">
		<div class="container">
			<?php
			/**
			 * Hook: woocommerce_before_main_content.
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 * @hooked WC_Structured_Data::generate_website_data() - 30
			 */
			do_action( 'woocommerce_before_main_content' );

			?>
			<div class="woocommerce-products-header">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="woocommerce-products-header__title page-title shopSection__title section__title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>

				<?php

				
				/**
				 * Hook: woocommerce_archive_description.
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */
				do_action( 'woocommerce_archive_description' );
				?>
			</div>
			<div class="shopSection__inner">       
				<div class="shopSection__categoryBlock categoryBlock">
					<div class="categoryBlock__products categoryBlockProducts">
						<div class="">
						<?php
						
						if ( woocommerce_product_loop() ) {

							/**
							 * Hook: woocommerce_before_shop_loop.
							 *
							 * @hooked woocommerce_output_all_notices - 10
							 * @hooked woocommerce_result_count - 20
							 * @hooked woocommerce_catalog_ordering - 30
							 */
							do_action( 'woocommerce_before_shop_loop' );

							woocommerce_product_loop_start();

							if ( wc_get_loop_prop( 'total' ) ) {
								while ( have_posts() ) {
									the_post();

									/**
									 * Hook: woocommerce_shop_loop.
									 */

									do_action( 'woocommerce_shop_loop' );

									wc_get_template_part( 'content', 'product' );
								}
							}

							woocommerce_product_loop_end();

							/**
							 * Hook: woocommerce_after_shop_loop.
							 *
							 * @hooked woocommerce_pagination - 10
							 */
							do_action( 'woocommerce_after_shop_loop' );
						} else {
							/**
							 * Hook: woocommerce_no_products_found.
							 *
							 * @hooked wc_no_products_found - 10
							 */
							do_action( 'woocommerce_no_products_found' );
						}

						/**
						 * Hook: woocommerce_after_main_content.
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						// do_action( 'woocommerce_after_main_content' );

						/**
						 * Hook: woocommerce_sidebar.
						 *
						 * @hooked woocommerce_get_sidebar - 10
						 */
						// do_action( 'woocommerce_sidebar' );
						?>
						</div>
						<!-- <button class="categoryBlockProducts__seeMore">
							показать еще
						</button> -->
					</div>
				</div>
				<aside class="shopSection__filters asideFilters">
					<div class="asideFilters__title">ФИЛЬТР ТОВАРОВ</div>
					<div class="asideFilters__priceFilter">
						<div class="asideFilters__heading">Цена</div>
					</div>
					<div class="asideFilters__selectBlock">
						<div class="asideFilters__heading">Материал</div>
						<select name="choice">
							<option value="first" selected>Выберите</option>
							<option value="second">Первый материал</option>
							<option value="third">Второй материал</option>
						</select>
					</div>
					<div class="asideFilters__selectBlock">
						<div class="asideFilters__heading">Цвет</div>
						<select name="choice">
							<option value="first" selected>Выберите</option>
							<option value="second">Черный</option>
							<option value="third">Красный</option>
						</select>
					</div>
					<div class="asideFilters__buttons filterButtonsBlock">
						<button class="filterButtonsBlock__btn btn--red">Показать</button>
						<button class="filterButtonsBlock__btn">Сбросить</button>
					</div>
				</aside>
			</div>
		</div>
	</section>


    
	<section class="bestsellers">
      <div class="container">
        <div class="bestsellers__titleWrapper promo__titleWrapper">
          <h2 class="bestsellers__title promo__title">хиты <br> продаж</h2>
        </div>
        <div class="bestsellers__slider bestsellersSlider">
          <?php $hits = get_field("hity", 27);?>
          <?php 
          foreach ($hits as $hit) {
            $product = wc_get_product($hit);
            //$shop_url = get_permalink(wc_get_page_id('shop'));
            $product_id = $product->get_ID();
            $product_sku = $product->get_SKU();
            $image_id  = $product->get_image_id();
            $img = wp_get_attachment_image_url( $image_id, 'full' );
            $title = $product->get_title();
            $desc = $product->get_short_description();
            $fulldesc = $product->get_description();
            ?>
            <div class="bestsellersSlider__item">
            <div class="product">
              <a href="<?= $product->get_permalink();?>" class="product__link">
                <div class="product__imgwrapper">
                  <img class="product__img" src="<?= $img?>" alt="" >
                </div>
              </a>
              <div class="product__title">
                <div class="product__name"><?= $title?></div>
                <div class="product__price">
                  <?php if($product) : ?>
                      <?php if($product->sale_price): ?>
                          <div class="price-old ">
                          <span>
                              <strong><?= $product->regular_price; ?></strong>
                              <span >p.</span>
                          </span>
                      </div>
                      <div class="price-current price-sale">
                          <strong> <?= $product->sale_price; ?></strong>
                          <span >p.</span>
                      </div>
                      <?php elseif ($product->regular_price): ?>
                      <div class="price-current">
                          <?= $product->regular_price; ?>
                          <span >p.</span>
                      </div>
                      <? else :?>
                      <div class="price-current">
                          <strong> Цена по запросу</strong>
                      </div>
                      <?php endif; ?>
                  <?php endif; ?>	
                </div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <a href="?add_to_wishlist=<?= $product_id; ?>" class="add_to_wishlist single_add_to_wishlist addToWL" data-product-id="<?= $product_id; ?>" data-product-type="simple" data-original-product-id="<?= $product_id; ?>" data-title="Добавить в список желаний" rel="nofollow">
                    <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    <!-- <span>test</span> -->
                  </a>
                  <a class="addedToWL d_none_custom" href="/wishlist/">В избранное</a>
                </div>
                <div class="customBtnToCartWrapper">
                  <a href="?add-to-cart=<?= $product_id; ?>" class="product__btn add_to_cart_button ajax_add_to_cart" data-product_id="<?= $product_id; ?>" data-product_sku="" aria-label="Добавить &quot;<?= $title; ?>&quot; в корзину" rel="nofollow">
                  В КОРЗИНУ
                </a>
                </div>
              </div>
            </div>
          </div>
          <?php
          }
          // $shortcode = '[products ids="' . implode(",", $hits) . '"]';
          // echo do_shortcode($shortcode);
          ?>
          

        </div>
        <div class="bestsellers__features bestsellersFeatures">
          <ul class="bestsellersFeatures__List">
            <li class="bestsellersFeatures__item"><span class="redText">100% гарантия </span>на продаваемый товар</li>
            <li class="bestsellersFeatures__item"><span class="redText">быстрая доставка</span> по всей стране</li>
            <li class="bestsellersFeatures__item"><span class="redText">качественный товар</span> и большой ассортимент</li>
          </ul>
        </div>
      </div>
    </section>
</main>


<?php
// get_sidebar();


get_footer( 'shop' );
