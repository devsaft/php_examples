<?php
/*
Template Name: Front Page Template
*/
?>

<?php
get_header();
?>

<main class="main">
    <section class="firstScreen">
      <div class="container">
        <div class="firstScreen__topContent topContent">
          <div class="topContent__left contentTopLeft">
            <h1 class="topContent__title">Premium outlet</h1>
            <div class="contentTopLeft__wrapper">
              <a href="/shop" class="topContent__link">В каталог</a>
              <h3 class="topContent__subtitle">Брендовые сумки</h3>
            </div>
          </div>
          <div class="topContent__right">
            <div class="topContent__imgWrapper">
              <img class="topContent__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/topSection-bg.jpg" alt="" >
            </div>
          </div>
        </div>
        <div class="firstScreen__bottomContent">
          <ul class="firstScreen__features features">
            <li class="firstScreen__featureItem">бесплатная доставка в дни заказа</li>
            <li class="firstScreen__featureItem">оплата после получения</li>
            <li class="firstScreen__featureItem">скидки до 60%</li>
          </ul>
        </div>
      </div>
    </section>
    <div class="categoriesBlock">
      <div class="container">
      <div class="categoriesBlock__inner">
          <a class="categoryItem" href="/product-category/pinko/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Pinko
            </div>
          </a>
          <a class="categoryItem" href="/product-category/marc-jacobs/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/marc-jacobs.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Marc Jacobs
            </div>
          </a>
          <a class="categoryItem" href="/product-category/michael-kors/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/michael-kors.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Michael Kors
            </div>
          </a>
          <a class="categoryItem" href="/product-category/louis-vuitton/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/Louis-vuitton.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Louis Vuitton
            </div>
          </a>
          <a class="categoryItem" href="/product-category/gucci/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/gucci.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Gucci
            </div>
          </a>
          <a class="categoryItem" href="/product-category/chanel/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/chanel.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Chanel
            </div>
          </a>
          <a class="categoryItem" href="/product-category/fendi/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/fendi.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Fendi
            </div>
          </a>
          <a class="categoryItem" href="/product-category/off-white/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/off-white.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Off White
            </div>
          </a>
          <a class="categoryItem" href="/product-category/christian-dior/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/christian-dior.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Christian Dior
            </div>
          </a>
          <a class="categoryItem" href="/product-category/bottega-veneta/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/botegga-venetta.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Bottega Veneta
            </div>
          </a>
          <a class="categoryItem" href="/product-category/coach/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/coach.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Coach
            </div>
          </a>
          <a class="categoryItem" href="/product-category/balenciaga/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/balenciaga.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Balenciaga
            </div>
          </a>
          <a class="categoryItem" href="/product-category/muzhsie-aksussuary/">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/man-accessouiries.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              мужсие аксуссуары
            </div>
          </a>         
        </div>
      </div>
    </div>
    <section class="bestsellers">
      <div class="container">
        <div class="bestsellers__titleWrapper promo__titleWrapper">
          <h2 class="bestsellers__title promo__title">хиты <br> продаж</h2>
        </div>
        <div class="bestsellers__slider bestsellersSlider">
          <?php $hits = get_field("hity");?>
          <?php 
          foreach ($hits as $hit) {
            $product = wc_get_product($hit);
            //$shop_url = get_permalink(wc_get_page_id('shop'));
            $product_id = $product->get_ID();
            $product_sku = $product->get_SKU();
            $image_id  = $product->get_image_id();
            $img = wp_get_attachment_image_url( $image_id, 'full' );
            $title = $product->get_title();
            $desc = $product->get_short_description();
            $fulldesc = $product->get_description();
            ?>
            <div class="bestsellersSlider__item">
            <div class="product">
              <a href="<?= $product->get_permalink();?>" class="product__link">
                <div class="product__imgwrapper">
                  <img class="product__img" src="<?= $img?>" alt="" >
                </div>
              </a>
              <div class="product__title">
                <div class="product__name"><?= $title?></div>
                <div class="product__price">
                  <?php if($product) : ?>
                      <?php if($product->sale_price): ?>
                          <div class="price-old ">
                          <span>
                              <strong><?= $product->regular_price; ?></strong>
                              <span >p.</span>
                          </span>
                      </div>
                      <div class="price-current price-sale">
                          <strong> <?= $product->sale_price; ?></strong>
                          <span >p.</span>
                      </div>
                      <?php elseif ($product->regular_price): ?>
                      <div class="price-current">
                          <?= $product->regular_price; ?>
                          <span >p.</span>
                      </div>
                      <? else :?>
                      <div class="price-current">
                          <strong> Цена по запросу</strong>
                      </div>
                      <?php endif; ?>
                  <?php endif; ?>	
                </div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <a href="?add_to_wishlist=<?= $product_id; ?>" class="add_to_wishlist single_add_to_wishlist addToWL" data-product-id="<?= $product_id; ?>" data-product-type="simple" data-original-product-id="<?= $product_id; ?>" data-title="Добавить в список желаний" rel="nofollow">
                    <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    <!-- <span>test</span> -->
                  </a>
                  <a class="addedToWL d_none_custom" href="/wishlist/">В избранное</a>
                </div>
                <div class="customBtnToCartWrapper">
                  <a href="?add-to-cart=<?= $product_id; ?>" class="product__btn add_to_cart_button ajax_add_to_cart" data-product_id="<?= $product_id; ?>" data-product_sku="" aria-label="Добавить &quot;<?= $title; ?>&quot; в корзину" rel="nofollow">
                  В КОРЗИНУ
                </a>
                </div>
              </div>
            </div>
          </div>
          <?php
          }
          // $shortcode = '[products ids="' . implode(",", $hits) . '"]';
          // echo do_shortcode($shortcode);
          ?>
          

        </div>
        <div class="bestsellers__features bestsellersFeatures">
          <ul class="bestsellersFeatures__List">
            <li class="bestsellersFeatures__item"><span class="redText">100% гарантия </span>на продаваемый товар</li>
            <li class="bestsellersFeatures__item"><span class="redText">быстрая доставка</span> по всей стране</li>
            <li class="bestsellersFeatures__item"><span class="redText">качественный товар</span> и большой ассортимент</li>
          </ul>
        </div>
      </div>
    </section>
  </main>

<?php
// get_sidebar();
get_footer();
