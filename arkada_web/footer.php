<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package arkada_web
 */

?>

    <!-- modal 1 -->
    <?php $admin_path =  admin_url() ;?>
      <script> var admin_path = "<?php echo $admin_path; ?>"; </script>
    <div class="md-modal md-effect-1" id="modal-1">
      <div class="md-content">
        <div class="cartBlock order">
          <div class="cartBlock__title">КОРЗИНА</div>
          <!-- <div class="cartBlock__subTitle">В КОРЗИНЕ 1 ТОВАР НА СУММУ 10 500 Р.</div> -->
          <div class="cartBlock__subtitle">В КОРЗИНЕ
            <div class="cartInner">

              <span class="js-bascetCountItems">
                <strong>
                  <? 
                    global $woocommerce;
                    // echo $woocommerce->cart->cart_contents_count;
                    $count = $woocommerce->cart->cart_contents_count; 
                    echo sprintf( _n( '%d товар', '%d товаров', $count, 'text domain' ), $count );
                  ?>
                </strong>
              </span> НА СУММУ 
              <strong>
                <span class="js-sumPrice">
                  <?
                    global $woocommerce;
                    $cartSum = $woocommerce->cart->get_cart_total();
                    echo $cartSum;
                  ?>
                </span>
              </strong>
            </div>
          </div>
          <div class="cartBlock__header cartBlockHeader">
            <div class="cartBlockHeader__item"></div>
            <div class="cartBlockHeader__item">фото</div>
            <div class="cartBlockHeader__item">НАЗВАНИЕ</div>
            <div class="cartBlockHeader__item">КОЛИЧЕСТВО</div>
            <div class="cartBlockHeader__item">ЦЕНА</div>
          </div>
          <div class="cartBlock__productsInner order__list">
              <?php
                  global $woocommerce;
                  $items = $woocommerce->cart->get_cart();

                      foreach($items as $item => $values) { 
                          // var_dump($values);
                          // echo '<br>';
                          $_product =  wc_get_product( $values['data']->get_id()); 
                          $_productId = $_product->get_id();
                          $delete_link = '/cart/?remove_item='.$item;
                          $_productTitle =$_product->get_title();
                          $_productQty = $values['quantity']; 
                          $_productWeight = $_product->get_weight(); 
                          $_productPrice = get_post_meta($values['product_id'] , '_price', true);
                          ?>

                            <div class="cartBlock__product cartBlockProduct order__item">
                              <div class="cartBlockProduct__item cartBlockProduct__item--close">
                                <div class="order__item-column order__item-column--delete">
                                  <a class="order__link-delete remove js-itemDelete" data-cart_item_key="<?= wc_get_cart_remove_url( $_productId );?>" data-product_sku="<?=$_product->get_sku();?>" data-product_id="<?=$_productId;?>" href="<?=get_site_url().$delete_link.'&amp;_wpnonce=0e9f7b388f';?>" aria-label="Удалить эту позицию" data-confirm="Удалить позицию?"></a>
                                </div>
                              </div>
                              <div class="cartBlockProduct__item cartBlockProduct__item--img">
                                <div class="cartBlock__imgWrapper">
                                  <?php echo $_product->get_image(); ?>
                                </div>
                              </div>
                              <div class="cartBlockProduct__item cartBlockProduct__item--title"><span class="redText"><?=$_productTitle;?></span></div>
                              <div class="cartBlockProduct__item cartBlockProduct__item--quantity">

                                <div class="order__item-column order__item-column--count productCardQuantity__block">
                                  <a class="order__item-count order__item-count--minus js-changeCount productCardQuantity__itemWrapper" href="#" data-num="-1">-</a>
                                  <input class="form__input form__input--count js-inputNum productCardQuantity__itemWrapper" name="count_id1" type="number" data-id="<?=$_productId;?>" value='<?=$_productQty;?>' min="1" max="99" readonly>
                                  <a class="order__item-count order__item-count--plus js-changeCount productCardQuantity__itemWrapper" href="#" data-num="+1">+</a>
                                </div>
                              </div>
                              <div class="cartBlockProduct__item cartBlockProduct__item--price">
                                <span class="redText"><?=$_productPrice;?> p.</span>
                              </div>
                            </div>
                          <?
                      } 
              ?>
          </div>
          <div class="cartBlock__total cartBlockTotal">
            <div class="cartBlockTotal__text ">итого: 
              <span class="js-sumPrice">
                <?
                  global $woocommerce;
                  $cartSum = $woocommerce->cart->get_cart_total();
                  echo $cartSum;
                ?>
              </span>
            </div>
            <button class="cartBlockTotal__btn">КУПИТЬ</button>
          </div>
        </div>
        <div class="checkoutBlock checkoutBlock--hidden order">
          <form class="checkoutBlock__form form">
            <label >Ваше имя*
              <input class="js-requireText" type="text" name="name" required>
            </label>
            <label >Ваш телефон*
              <input class="js-requirePhone" type="tel" name="phone" required>
            </label>
            <label >Адрес доставки*
              <input type="text" name="address">
            </label>
            <button class="checkoutBlock__btn cartBlockTotal__btn" type="submit" >Заказать</button>
          </form>
        </div>
        <div class="checkoutSuccess checkoutSuccess--hidden">
          <div class="checkoutSuccess__text">
            Заявка успешно принята. Мы свяжемся с вами для уточнения деталей.
          </div>
        </div>
        <div class="cartBlock__crossWrapper">
          <div class="cartBlock__cross cartBlockCross md-close">
            <div class="cartBlockCross__line"></div>
            <div class="cartBlockCross__line"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="md-overlay"></div>
    <!-- modal 1 end -->



  
  
  
  
    <footer class="footer">
    <div class="container">
      <div class="footer__top footerTop">
        <div class="footerTop__mainContent">
          <?php 
            wp_nav_menu( array(
              'menu'            => '',              // (string) Название выводимого меню (указывается в админке при создании меню, приоритетнее 
                                  // чем указанное местоположение theme_location - если указано, то параметр theme_location игнорируется)
              'container'       => 'ul',           // (string) Контейнер меню. Обворачиватель ul. Указывается тег контейнера (по умолчанию в тег div)
              'container_class' => 'footerTop__listWrapper',              // (string) class контейнера (div тега)
              'container_id'    => '',              // (string) id контейнера (div тега)
              'menu_class'      => 'footerTop__list',          // (string) class самого меню (ul тега)
              'menu_id'         => '',              // (string) id самого меню (ul тега)
              'echo'            => true,            // (boolean) Выводить на экран или возвращать для обработки
              'fallback_cb'     => 'wp_page_menu',  // (string) Используемая (резервная) функция, если меню не существует (не удалось получить)
              'before'          => '',              // (string) Текст перед <a> каждой ссылки
              'after'           => '',              // (string) Текст после </a> каждой ссылки
              'link_before'     => '',              // (string) Текст перед анкором (текстом) ссылки
              'link_after'      => '',              // (string) Текст после анкора (текста) ссылки
              'depth'           => 0,               // (integer) Глубина вложенности (0 - неограничена, 2 - двухуровневое меню)
              'walker'          => '',              // (object) Класс собирающий меню. Default: new Walker_Nav_Menu
              'theme_location'  => 'footerTop__list'               // (string) Расположение меню в шаблоне. (указывается ключ которым было зарегистрировано меню в функции register_nav_menus)
            ) );
          ?>
        

          <div class="footerTop__contacts footerContacts">
            <div class="footerContacts__block">
              <div class="footerContacts__title"><span class="redText">АДРЕС:</span></div>
              <div class="footerContacts__text"> 
                Москва, ул. Кировоградская., 13A,
                Торговый центр "Columbus" (Склад) </div>
            </div>
            <div class="footerContacts__block">
              <div class="footerContacts__title"><span class="redText">ТЕЛЕФОН:</span></div>
              <div class="footerContacts__text"><a class="footerContacts__link" href="tel:+79207670990">+7 (920) 767-09-90</a></div>
              <div class="footerContacts__title"><span class="redText">EMAIL:</span></div>
              <div class="footerContacts__text"><a class="footerContacts__link" href="mailto:premium.outlet@yandex.ru">premium.outlet@yandex.ru</a></div>
              <div class="footerContacts__social footerContactsSocial">
                <a class="footerContactsSocial__link" href="https://www.instagram.com/premium_style_rf" >
                  <img class="footerContactsSocial__img" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/inst.svg" alt="" >
                </a>
                <a class="footerContactsSocial__link" href="https://wa.me/+79207670990" >
                  <img class="footerContactsSocial__img" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/whatsapp.svg" alt="" >
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footerTop__ImgWrapper">
      <img class="footerTop__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/footer-bg.jpg" alt="" >
    </div>
    <div class="footer__bottom footerBottom">
      <div class="footerBottom__copyright">© 2021 Все права защищены.</div>
    </div>
  </footer>


<?php wp_footer(); ?>

</body>
</html>
