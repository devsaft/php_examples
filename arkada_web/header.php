<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package arkada_web
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
  <!-- fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700;800;900&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<header class="header">
    <div class="container">
      <div class="header__inner">
        <div class="catalogBox header__catalogBox">
          <btn class="catalogBox__btn">
            <span class="catalogBox__btn-line"></span>
          </btn>
          <?php 
          wp_nav_menu( array(
            'menu'            => '',              // (string) Название выводимого меню (указывается в админке при создании меню, приоритетнее 
                                // чем указанное местоположение theme_location - если указано, то параметр theme_location игнорируется)
            'container'       => 'div',           // (string) Контейнер меню. Обворачиватель ul. Указывается тег контейнера (по умолчанию в тег div)
            'container_class' => 'catalogBox__listWrapper',              // (string) class контейнера (div тега)
            'container_id'    => '',              // (string) id контейнера (div тега)
            'menu_class'      => 'catalogBox__list',          // (string) class самого меню (ul тега)
            'menu_id'         => '',              // (string) id самого меню (ul тега)
            'echo'            => true,            // (boolean) Выводить на экран или возвращать для обработки
            'fallback_cb'     => 'wp_page_menu',  // (string) Используемая (резервная) функция, если меню не существует (не удалось получить)
            'before'          => '',              // (string) Текст перед <a> каждой ссылки
            'after'           => '',              // (string) Текст после </a> каждой ссылки
            'link_before'     => '',              // (string) Текст перед анкором (текстом) ссылки
            'link_after'      => '',              // (string) Текст после анкора (текста) ссылки
            'depth'           => 0,               // (integer) Глубина вложенности (0 - неограничена, 2 - двухуровневое меню)
            'walker'          => '',              // (object) Класс собирающий меню. Default: new Walker_Nav_Menu
            'theme_location'  => 'catalog-list'               // (string) Расположение меню в шаблоне. (указывается ключ которым было зарегистрировано меню в функции register_nav_menus)
          ) );
        ?>
          <!-- <div class="catalogBox__listWrapper">
            <ul class="catalogBox__list">
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Pinko</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Marc Jacobs</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Michael Kors</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Louis Vuitton</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Gucci</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Chanel</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Fendi</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Off White</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Christian Dior</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Bottega Veneta</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Coach</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >Balenciaga</a>
              </li>
              <li class="catalogBox__item">
                <a class="catalogBox__link" href="#" >мужсие аксуссуары</a>
              </li>
            </ul>
          </div> -->
        </div>
        <?php 
          wp_nav_menu( array(
            'menu'            => '',              // (string) Название выводимого меню (указывается в админке при создании меню, приоритетнее 
                                // чем указанное местоположение theme_location - если указано, то параметр theme_location игнорируется)
            'container'       => 'nav',           // (string) Контейнер меню. Обворачиватель ul. Указывается тег контейнера (по умолчанию в тег div)
            'container_class' => 'header__nav',              // (string) class контейнера (div тега)
            'container_id'    => '',              // (string) id контейнера (div тега)
            'menu_class'      => 'nav__list',          // (string) class самого меню (ul тега)
            'menu_id'         => '',              // (string) id самого меню (ul тега)
            'echo'            => true,            // (boolean) Выводить на экран или возвращать для обработки
            'fallback_cb'     => 'wp_page_menu',  // (string) Используемая (резервная) функция, если меню не существует (не удалось получить)
            'before'          => '',              // (string) Текст перед <a> каждой ссылки
            'after'           => '',              // (string) Текст после </a> каждой ссылки
            'link_before'     => '',              // (string) Текст перед анкором (текстом) ссылки
            'link_after'      => '',              // (string) Текст после анкора (текста) ссылки
            'depth'           => 0,               // (integer) Глубина вложенности (0 - неограничена, 2 - двухуровневое меню)
            'walker'          => '',              // (object) Класс собирающий меню. Default: new Walker_Nav_Menu
            'theme_location'  => 'header-nav'               // (string) Расположение меню в шаблоне. (указывается ключ которым было зарегистрировано меню в функции register_nav_menus)
          ) );
        ?>
        <!-- <nav class="header__nav nav">
          <ul class="nav__list">
            <li class="nav__item">
              <a href="shop.html" class="nav__link">
                КАТАЛОГ
              </a>
            </li>
            <li class="nav__item">
              <a href="#" class="nav__link">
                Доставка и оплата
              </a>
            </li>
            <li class="nav__item">
              <a href="#" class="nav__link">
                О нас
              </a>
            </li>
            <li class="nav__item">
              <a href="#" class="nav__link">
                Гарантии
              </a>
            </li>
            <li class="nav__item">
              <a href="#" class="nav__link">
                Отзывы
              </a>
            </li>
          </ul>
        </nav> -->
        <div class="header__social social">
          <a href="https://wa.me/+79207670990" class="social__link">
            <div class="social__text">НАПИСАТЬ НА WHATSAPP</div>
            <img class="social__img" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/logo_whatsapp.svg" alt="" >
          </a>
        </div>
        <div class="header__userBox userBox">
          <a class="userBox__link" href="/my-account/" >
            <img class="userBox__img" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-user.svg" alt="" >
          </a>
          <a class="userBox__link" href="/wishlist" >
            <img class="userBox__img" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like1.svg" alt="" >
          </a>
          <a class="userBox__link modalTrigger" href="#" >
            <img class="userBox__img" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-cart.svg" alt="" >
          </a>
        </div>
      </div>
    </div>
  </header>
