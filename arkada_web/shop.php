<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package arkada_web
 */

get_header();
?>

<main class="main">
    <section class="firstScreen">
      <div class="container">
        <div class="firstScreen__topContent topContent">
          <div class="topContent__left contentTopLeft">
            <h1 class="topContent__title">Premium outlet2</h1>
            <div class="contentTopLeft__wrapper">
              <a href="#" class="topContent__link">В каталог</a>
              <h3 class="topContent__subtitle">Брендовые сумки</h3>
            </div>
          </div>
          <div class="topContent__right">
            <div class="topContent__imgWrapper">
              <img class="topContent__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/topSection-bg.jpg" alt="" >
            </div>
          </div>
        </div>
        <div class="firstScreen__bottomContent">
          <ul class="firstScreen__features features">
            <li class="firstScreen__featureItem">бесплатная доставка в дни заказа</li>
            <li class="firstScreen__featureItem">оплата после получения</li>
            <li class="firstScreen__featureItem">скидки до 60%</li>
          </ul>
        </div>
      </div>
    </section>
    <div class="categoriesBlock">
      <div class="container">
        <div class="categoriesBlock__inner">
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Pinko
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Marc Jacobs
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Michael Kors
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Louis Vuitton
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Gucci
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Chanel
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Fendi
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Off White
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Christian Dior
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Bottega Veneta
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Coach
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              Balenciaga
            </div>
          </a>
          <a class="categoryItem" href="#">
            <div class="categoryItem__imgWrapper">
              <img class="categoryItem__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/categories/pinko.jpg" alt="" >
            </div>
            <div class="categoryItem__title">
              мужсие аксуссуары
            </div>
          </a>         
        </div>
      </div>
    </div>
    <section class="bestsellers">
      <div class="container">
        <div class="bestsellers__titleWrapper promo__titleWrapper">
          <h2 class="bestsellers__title promo__title">хиты <br> продаж</h2>
        </div>
        <div class="bestsellers__slider bestsellersSlider">
          <div class="bestsellersSlider__item">
            <div class="product">
              <div class="product__imgwrapper">
                <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
              </div>
              <div class="product__title">
                <div class="product__name">Michael Kors</div>
                <div class="product__price">2 500 р.</div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                </div>
                <button class="product__btn">В КОРЗИНУ</button>
              </div>
            </div>
          </div>
          <div class="bestsellersSlider__item">
            <div class="product">
              <div class="product__imgwrapper">
                <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
              </div>
              <div class="product__title">
                <div class="product__name">Michael Kors</div>
                <div class="product__price">2 500 р.</div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                </div>
                <button class="product__btn">В КОРЗИНУ</button>
              </div>
            </div>
          </div>
          <div class="bestsellersSlider__item">
            <div class="product">
              <div class="product__imgwrapper">
                <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
              </div>
              <div class="product__title">
                <div class="product__name">Michael Kors</div>
                <div class="product__price">2 500 р.</div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                </div>
                <button class="product__btn">В КОРЗИНУ</button>
              </div>
            </div>
          </div>
          <div class="bestsellersSlider__item">
            <div class="product">
              <div class="product__imgwrapper">
                <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
              </div>
              <div class="product__title">
                <div class="product__name">Michael Kors</div>
                <div class="product__price">2 500 р.</div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                </div>
                <button class="product__btn">В КОРЗИНУ</button>
              </div>
            </div>
          </div>
          <div class="bestsellersSlider__item">
            <div class="product">
              <div class="product__imgwrapper">
                <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
              </div>
              <div class="product__title">
                <div class="product__name">Michael Kors</div>
                <div class="product__price">2 500 р.</div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                </div>
                <button class="product__btn">В КОРЗИНУ</button>
              </div>
            </div>
          </div>
          <div class="bestsellersSlider__item">
            <div class="product">
              <div class="product__imgwrapper">
                <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
              </div>
              <div class="product__title">
                <div class="product__name">Michael Kors</div>
                <div class="product__price">2 500 р.</div>
              </div>
              <div class="product__subtitle">
                <div class="product__likeImgWrapper">
                  <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                </div>
                <button class="product__btn">В КОРЗИНУ</button>
              </div>
            </div>
          </div>

        </div>
        <div class="bestsellers__features bestsellersFeatures">
          <ul class="bestsellersFeatures__List">
            <li class="bestsellersFeatures__item"><span class="redText">100% гарантия </span>на продаваемый товар</li>
            <li class="bestsellersFeatures__item"><span class="redText">быстрая доставка</span> по всей стране</li>
            <li class="bestsellersFeatures__item"><span class="redText">качественный товар</span> и большой ассортимент</li>
          </ul>
        </div>
      </div>
    </section>
  </main>

<?php
// get_sidebar();
get_footer();
