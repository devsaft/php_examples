<?php
/**
 * arkada_web functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package arkada_web
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'arkada_web_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function arkada_web_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on arkada_web, use a find and replace
		 * to change 'arkada_web' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'arkada_web', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'arkada_web' ),
				'header-nav' => 'Навигация в шапке',      //Название другого месторасположения меню в шаблоне
				'catalog-list' => 'Список категорий',      //Название другого месторасположения меню в шаблоне
				'footerTop__list' => 'Меню в футере',      //Название другого месторасположения меню в шаблоне
				)
		);
		// menu header


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'arkada_web_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'arkada_web_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function arkada_web_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'arkada_web_content_width', 640 );
}
add_action( 'after_setup_theme', 'arkada_web_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function arkada_web_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'arkada_web' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'arkada_web' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'arkada_web_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function arkada_web_scripts() {
	wp_enqueue_style( 'arkada_web-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'arkada_web-style', 'rtl', 'replace' );

	wp_enqueue_script( 'arkada_web-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'arkada_web_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


add_action('wp_enqueue_scripts', 'get_styles');
add_action('wp_enqueue_scripts', 'get_scripts');


function get_styles() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_style('main-css', get_template_directory_uri() . '/assets/css/style.min.css');
	// wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	// wp_enqueue_style('bootstrap-grid.min', get_template_directory_uri() . '/assets/css/bootstrap-grid.min.css');
	// wp_enqueue_style('bootstrap-reboot.min', get_template_directory_uri() . '/assets/css/bootstrap-reboot.min.css');
	wp_enqueue_style('slick-css', get_template_directory_uri() . '/assets/libraries/css/slick.css');
	wp_enqueue_style('slick-theme-css', get_template_directory_uri() . '/assets/libraries/css/slick-theme.css');
	wp_enqueue_style('fancybox-css', get_template_directory_uri() . '/libraries/css/fancybox.min.css');
}

function get_scripts() {
	wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/assets/libraries/js/jquery.js');
	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/libraries/js/slick.min.js');
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/assets/libraries/js/jquery.fancybox.min.js');
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/main.min.js');
	// wp_enqueue_script( 'bootstrap.min', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
	// wp_enqueue_script( 'bootstrap.bundle.min', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js');
}


/**
 * Turn on woocommerce
 */
if ( class_exists( 'WooCommerce' ) ) {
	require_once(get_template_directory() . '/woo-c.php');
	}

	remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
	remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	// remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
	// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	
	
	
/**
 * Display products loop
 */
	add_action('woocommerce_before_shop_loop_item', 'another_kind_of_loop');
	
	function another_kind_of_loop()
	{
			$product = wc_get_product(get_the_ID());
			//$shop_url = get_permalink(wc_get_page_id('shop'));
			$product_id = $product->get_ID();
			$product_sku = $product->get_SKU();
			$image_id  = $product->get_image_id();
			$img = wp_get_attachment_image_url( $image_id, 'full' );
			$title = $product->get_title();
			$desc = $product->get_short_description();
			$fulldesc = $product->get_description(); 
			if ($product) {
	?>      
	
		<div class="product">
			<a href="<?= $product->get_permalink();?>" class="product__link">
				<div class="product__imgwrapper">
					<img class="product__img" src="<?= $img;?>" alt="" >
				</div>
				
			</a>
			<div class="product__desc d_none_custom">
				<?php echo $fulldesc; ?>
			</div>
			<div class="product__title">
					<div class="product__name"><?=$title;?></div>
					<div class="product__price">
						<?php if($product) : ?>
								<?php if($product->sale_price): ?>
										<div class="price-old ">
										<span>
												<strong><?= $product->regular_price; ?></strong>
												<span >p.</span>
										</span>
								</div>
								<div class="price-current price-sale">
										<strong> <?= $product->sale_price; ?></strong>
										<span >p.</span>
								</div>
								<?php elseif ($product->regular_price): ?>
								<div class="price-current">
										<?= $product->regular_price; ?>
										<span >p.</span>
								</div>
								<? else :?>
								<div class="price-current">
										<strong> Цена по запросу</strong>
								</div>
								<?php endif; ?>
						<?php endif; ?>	
					</div>
				</div>
			<div class="product__subtitle">
				<div class="product__likeImgWrapper">
					<a href="?add_to_wishlist=<?= $product_id; ?>" class="add_to_wishlist single_add_to_wishlist addToWL" data-product-id="<?= $product_id; ?>" data-product-type="simple" data-original-product-id="<?= $product_id; ?>" data-title="Добавить в список желаний" rel="nofollow">
						<img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
						<!-- <span>test</span> -->
					</a>
					<a class="addedToWL d_none_custom" href="/wishlist/">В избранное</a>
				</div>
				<div class="customBtnToCartWrapper">
					<a href="?add-to-cart=<?= $product_id; ?>" class="product__btn add_to_cart_button ajax_add_to_cart" data-product_id="<?= $product_id; ?>" data-product_sku="" aria-label="Добавить &quot;<?= $title; ?>&quot; в корзину" rel="nofollow">
					В КОРЗИНУ
				</a>
				</div>
			</div>
		</div>
			
			<?php
			}
	}


/**
 * Products per page dropdown
 */
// Lets create the function to house our form
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
add_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 25);

function woocommerce_catalog_page_ordering() {
    ?>
    <?php echo '<span class="itemsorder itemsorder--custom">' ?>
    <form action="" method="POST" name="results" class="woocommerce-ordering woocommerce-ordering--custom">
        <select name="woocommerce-sort-by-columns" id="woocommerce-sort-by-columns" class="sortby"
                onchange="this.form.submit()">
            <?php
 
            //Get products on page reload
            if ( isset( $_POST['woocommerce-sort-by-columns'] ) && ( ( $_COOKIE['shop_pageResults'] <> $_POST['woocommerce-sort-by-columns'] ) ) ) {
                $numberOfProductsPerPage = $_POST['woocommerce-sort-by-columns'];
            } else {
                $numberOfProductsPerPage = $_COOKIE['shop_pageResults'];
            }
 
            //  This is where you can change the amounts per page that the user will use. feel free to change the numbers and text as you want.
            $shopCatalog_orderby = apply_filters( 'woocommerce_sortby_page', array(
                //Add as many of these as you like, -1 shows all products per page
                //  ''       => __('Results per page', 'woocommerce'),
                '15' => __( 'Показывать: 15', 'ignition-child' ),
                '30' => __( 'Показывать: 30', 'ignition-child' ),
                '-1' => __( 'Показать все товары', 'ignition-child' ),
            ) );
 
            foreach ( $shopCatalog_orderby as $sort_id => $sort_name ) {
                echo '<option value="' . $sort_id . '" ' . selected( $numberOfProductsPerPage, $sort_id, true ) . ' >' . $sort_name . '</option>';
            }
            ?>
        </select>
    </form>
 
    <?php echo ' </span>' ?>
		
    <?php
}
 
// now we set our cookie if we need to
function dl_sort_by_page( $count ) {
    $count = 15; // Number of products per page
    if ( isset( $_COOKIE['shop_pageResults'] ) ) { // if normal page load with cookie
        $count = $_COOKIE['shop_pageResults'];
    }
    if ( isset( $_POST['woocommerce-sort-by-columns'] ) ) { //if form submitted
        setcookie( 'shop_pageResults', $_POST['woocommerce-sort-by-columns'], time() + 1209600, '/', $_SERVER['HTTP_HOST'], false ); //this will fail if any part of page has been output- hope this works!
        $count = $_POST['woocommerce-sort-by-columns'];
    }
 
    // else normal page load and no cookie
    return $count;
}
 
add_filter( 'loop_shop_per_page', 'dl_sort_by_page' );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_page_ordering', 25 );



/**
 * Remove the result count from WooCommerce
 */
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );



/**
 * Display grid_or_list block
 */
function grid_or_list_function (){ ?>
		<div class="categoryView__block categoryView__block--viewSwitcher categoryViewBlock">
			<div class="categoryViewBlock__title">Вид:</div>
			<div class="categoryViewBlock__item categoryViewBlock__list shopListViewBtn">
				<span class="shopListViewBtn__item"></span>
				<span class="shopListViewBtn__item"></span>
				<span class="shopListViewBtn__item"></span>
				<span class="shopListViewBtn__item"></span>
			</div>
			<div class="categoryViewBlock__item categoryViewBlock__grid shopGridViewBtn">
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
				<span class="shopGridViewBtn__item"></span>
			</div>
		</div>
	<?php
}

add_action( 'woocommerce_before_shop_loop', 'grid_or_list_function', 30 );



/**
 * Add custom wrapper for before_shop_loop
 */
function before_shop_loop_wrapper(){
	?>
		<div class="categoryBlock__view before_shop_loop_wrapper--custom">
	<?php
}

function after_shop_loop_wrapper(){
	?>
		</div>
	<?php
}

add_action( 'woocommerce_before_shop_loop', 'before_shop_loop_wrapper', 10 );
add_action( 'woocommerce_before_shop_loop', 'after_shop_loop_wrapper', 40 );



/**
 * Add custom currency symbol to admin panel
 */
add_filter( 'woocommerce_currencies', 'add_cw_currency' );
function add_cw_currency( $cw_currency ) {
     $cw_currency['Российский рубль маленький'] = __( 'Российский рубль маленький', 'woocommerce' );
     return $cw_currency;
}
add_filter('woocommerce_currency_symbol', 'add_cw_currency_symbol', 10, 2);
function add_cw_currency_symbol( $custom_currency_symbol, $custom_currency ) {
     switch( $custom_currency ) {
         case 'Российский рубль маленький': $custom_currency_symbol = 'р.'; break;
     }
     return $custom_currency_symbol;
}


/**
 * Cart functions
 */
add_action('wp_ajax_updateCart', 'updateCart');
add_action('wp_ajax_nopriv_updateCart', 'updateCart');

function updateCart() {
  global $woocommerce;
  $cartCounter =  $woocommerce->cart->cart_contents_count;
  $cartSum = $woocommerce->cart->get_cart_total();
	$customCartSumTitle = sprintf( _n( '%d товар', '%d товаров', $cartCounter, 'text domain' ), $cartCounter );
  $newCartInner = 
		'<span class="js-bascetCountItems"><strong>'
		.$customCartSumTitle
		.'</strong></span> НА СУММУ <strong> <span class="js-bascetPrice js-sumPrice">'
		.$cartSum
		.'</span></strong>';
	echo $newCartInner;
  wp_die();
};


add_action('wp_ajax_removeFromCart', 'removeFromCart');
add_action('wp_ajax_nopriv_removeFromCart', 'removeFromCart');

function removeFromCart() {
  $cart = WC()->instance()->cart;
    $id = $_POST['product_id'];
    $cart_id = $cart->generate_cart_id($id);
    $cart_item_id = $cart->find_product_in_cart($cart_id);

    if($cart_item_id){
       $cart->set_quantity($cart_item_id,0);
    }
    updateCart();
};

add_action('wp_ajax_updateModalCart', 'updateModalCart');
add_action('wp_ajax_nopriv_updateModalCart', 'updateModalCart');

function updateModalCart() {
  global $woocommerce;
  $items = $woocommerce->cart->get_cart();

	foreach($items as $item => $values) { 
		// var_dump($values);
		// echo '<br>';
		$_product =  wc_get_product( $values['data']->get_id()); 
		$_productId = $_product->get_id();
		$delete_link = '/cart/?remove_item='.$item;
		$_productTitle =$_product->get_title();
		$_productQty = $values['quantity']; 
		$_productWeight = $_product->get_weight(); 
		$_productPrice = get_post_meta($values['product_id'] , '_price', true);
		?>

			<div class="cartBlock__product cartBlockProduct order__item">
				<div class="cartBlockProduct__item cartBlockProduct__item--close">
					<div class="order__item-column order__item-column--delete">
						<a class="order__link-delete remove js-itemDelete" data-cart_item_key="<?= wc_get_cart_remove_url( $_productId );?>" data-product_sku="<?=$_product->get_sku();?>" data-product_id="<?=$_productId;?>" href="<?=get_site_url().$delete_link.'&amp;_wpnonce=0e9f7b388f';?>" aria-label="Удалить эту позицию" data-confirm="Удалить позицию?"></a>
					</div>
				</div>
				<div class="cartBlockProduct__item cartBlockProduct__item--img">
					<div class="cartBlock__imgWrapper">
						<?php echo $_product->get_image(); ?>
					</div>
				</div>
				<div class="cartBlockProduct__item cartBlockProduct__item--title"><span class="redText"><?=$_productTitle;?></span></div>
				<div class="cartBlockProduct__item cartBlockProduct__item--quantity">

					<div class="order__item-column order__item-column--count productCardQuantity__block">
						<a class="order__item-count order__item-count--minus js-changeCount productCardQuantity__itemWrapper" href="#" data-num="-1">-</a>
						<input class="form__input form__input--count js-inputNum productCardQuantity__itemWrapper" name="count_id1" type="number" data-id="<?=$_productId;?>" value='<?=$_productQty;?>' min="1" max="99" readonly>
						<a class="order__item-count order__item-count--plus js-changeCount productCardQuantity__itemWrapper" href="#" data-num="+1">+</a>
					</div>
				</div>
				<div class="cartBlockProduct__item cartBlockProduct__item--price">
					<span class="redText"><?=$_productPrice;?> p.</span>
				</div>
			</div>
		<?
} 
      
  wp_die();
};

add_action('wp_ajax_updateTotalCount', 'updateTotalCount');
add_action('wp_ajax_nopriv_updateTotalCount', 'updateTotalCount');

function updateTotalCount() {
  global $woocommerce;
  $cartSum = $woocommerce->cart->get_cart_total();
  echo $cartSum;
  wp_die();
};

add_action('wp_ajax_updateSingleItemQty', 'updateSingleItemQty');
add_action('wp_ajax_nopriv_updateSingleItemQty', 'updateSingleItemQty');

function updateSingleItemQty() {
  $id = $_POST['productId'];
  $newAmount = $_POST['newAmount'];
  global $woocommerce;
  $WC_Cart = WC()->cart->get_cart();
  foreach ($WC_Cart as $cartItem) {
    if ($cartItem["product_id"] == $id) {
      WC()->cart->remove_cart_item( $cartItem["key"] );
    }
  }
  WC()->cart->add_to_cart( $id, $newAmount );
  $cartSum = $woocommerce->cart->get_cart_total();
  echo $cartSum;
  wp_die();
}

add_action('wp_ajax_create_vip_order', 'create_vip_order');
add_action('wp_ajax_nopriv_create_vip_order', 'create_vip_order');
function create_vip_order() {
  $order = $_POST['order'];

  $cart = WC()->cart;

	$phone = esc_attr( trim( $order['clientPhone'] ) );
	$email = esc_attr( trim( '' ) );
	$name = esc_attr( trim( $order['clientName'] ) );

	$address = [
		'first_name' => $name,
		'email'      => $email,
		'phone'      => $phone,
		'country'    => 'RU',
	];

  $customer_note = '';

  if (trim($order['clientPickup'])== '1') {
    $customer_note = $customer_note.'<p>Заказ на самовывоз</p>';
  }
  else {
    $customer_note = $customer_note.'<p>Заказ на доставку по адресу: '.$order['clientAddress'].'</p>';
  }

  if (trim($order['clientDelivery']) == '1') {
    $customer_note = $customer_note.'<p>В ближайшее время</p>';
  }
  elseif (trim($order['clientDelivery']) == '2') {
    $customer_note = $customer_note.'<p>Заказ на '.$order['clientData'].' к '.$order['clientTime_H'].' часам '.$order['clientTime_M'].' минутам</p>';
  } 
  
  if (trim($order['clientPayment']) == '1') {
    $customer_note = $customer_note.'<p>Оплата наличными</p>';
  }
  elseif (trim($order['clientPayment']) == '2') {
    $customer_note = $customer_note.'<p>Оплата картой при доставке</p>';
  }

  $args = array(
    'customer_note' => $customer_note,
  );

	$order = wc_create_order($args);

	// Информация о покупателе
	$order->set_address( $address, 'billing' );
	$order->set_address( $address, 'shipping' );

	// Товары из корзины
	foreach( $cart->get_cart() as $cart_item_key => $cart_item ) {

		$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

		$order->add_product( $_product, $cart_item['quantity'], [
			'variation' => $cart_item['variation'],
			'totals'    => [
				'subtotal'     => $cart_item['line_subtotal'],
				'subtotal_tax' => $cart_item['line_subtotal_tax'],
				'total'        => $cart_item['line_total'],
				'tax'          => $cart_item['line_tax'],
				'tax_data'     => $cart_item['line_tax_data']
			]
		]);
	}

	// Добавить купоны
	foreach ( $cart->get_coupons() as $code => $coupon ) {
		$order->add_coupon( $code, $cart->get_coupon_discount_amount( $code ), $cart->get_coupon_discount_tax_amount( $code ) );
	}

	$order->calculate_totals();

	// Отправить письмо юзеру
	$mailer = WC()->mailer();
	$email = $mailer->emails['WC_Email_Customer_Processing_Order'];
	$email->trigger( $order->id );

	// Отправить письмо админу
	$email = $mailer->emails['WC_Email_New_Order'];
	$email->trigger( $order->id );

	// Очистить корзину
	$cart->empty_cart();

	wp_send_json_success( $order->id );
}
/**
 * Cart functions end
 */


/**
 * Add photo to email template
 */
// function cw_add_wc_order_email_images( $output, $order ) {
// 	static $run = 0;
// 	if ( $run ) {
// 			 return $output;
// 	 }
// 	$args = array(
// 			 'order'                 => $order,
// 			 'items'                 => $order->get_items(),
// 			 'show_download_links'   => $show_download_links,
// 			 'show_sku'              => $show_sku,
// 			 'show_purchase_note'    => $show_purchase_note,
// 			 'show_image'   	=> true,
// 			 'image_size'    => array( 100, 50 )
// 	 );
// 	$run++;
// 	return $order->email_order_items_table( $args );
// }
// add_filter( 'woocommerce_email_order_items_table', 'cw_add_wc_order_email_images' );
// Adds image to WooCommerce order emails

// function w3p_add_image_to_wc_emails( $args ) {
//     $args['show_image'] = true;
//     // $args['image_size'] = array( 100, 50 );
//     return $args;
// }

// add_filter( 'woocommerce_email_order_items_args', 'w3p_add_image_to_wc_emails' );