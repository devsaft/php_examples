<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package arkada_web
 */

get_header();
?>

  <main class="main">
    <section class="shopSection">
      <div class="container">
        <h1 class="shopSection__title section__title">сумки женские <span class="redText">pinko</span></h1>
        <div class="shopSection__inner">       
          <div class="shopSection__categoryBlock categoryBlock">
            <div class="categoryBlock__view categoryView">
              <div class="categoryView__block categoryViewBlock">
                <select name="choice">
                  <option value="first">First Value</option>
                  <option value="second" selected>Second Value</option>
                  <option value="third">Third Value</option>
                </select>
              </div>
              <div class="categoryView__block categoryViewBlock">
                <select name="choice">
                  <option value="first">First Value</option>
                  <option value="second" selected>Second Value</option>
                  <option value="third">Third Value</option>
                </select>
              </div>
              <div class="categoryView__block categoryViewBlock">
                <div class="categoryViewBlock__title">Вид:</div>
                <div class="categoryViewBlock__item categoryViewBlock__line"></div>
                <div class="categoryViewBlock__item categoryViewBlock__grid"></div>
              </div>
            </div>
            <div class="categoryBlock__products categoryBlockProducts">
              <div class="categoryBlockProducts__Inner">
                <div class="product">
                  <div class="product__imgwrapper">
                    <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
                  </div>
                  <div class="product__title">
                    <div class="product__name">Michael Kors</div>
                    <div class="product__price">2 500 р.</div>
                  </div>
                  <div class="product__subtitle">
                    <div class="product__likeImgWrapper">
                      <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    </div>
                    <button class="product__btn">В КОРЗИНУ</button>
                  </div>
                </div>
                <div class="product">
                  <div class="product__imgwrapper">
                    <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
                  </div>
                  <div class="product__title">
                    <div class="product__name">Michael Kors</div>
                    <div class="product__price">2 500 р.</div>
                  </div>
                  <div class="product__subtitle">
                    <div class="product__likeImgWrapper">
                      <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    </div>
                    <button class="product__btn">В КОРЗИНУ</button>
                  </div>
                </div>
                <div class="product">
                  <div class="product__imgwrapper">
                    <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
                  </div>
                  <div class="product__title">
                    <div class="product__name">Michael Kors</div>
                    <div class="product__price">2 500 р.</div>
                  </div>
                  <div class="product__subtitle">
                    <div class="product__likeImgWrapper">
                      <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    </div>
                    <button class="product__btn">В КОРЗИНУ</button>
                  </div>
                </div>
                <div class="product">
                  <div class="product__imgwrapper">
                    <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
                  </div>
                  <div class="product__title">
                    <div class="product__name">Michael Kors</div>
                    <div class="product__price">2 500 р.</div>
                  </div>
                  <div class="product__subtitle">
                    <div class="product__likeImgWrapper">
                      <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    </div>
                    <button class="product__btn">В КОРЗИНУ</button>
                  </div>
                </div>
                <div class="product">
                  <div class="product__imgwrapper">
                    <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
                  </div>
                  <div class="product__title">
                    <div class="product__name">Michael Kors</div>
                    <div class="product__price">2 500 р.</div>
                  </div>
                  <div class="product__subtitle">
                    <div class="product__likeImgWrapper">
                      <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    </div>
                    <button class="product__btn">В КОРЗИНУ</button>
                  </div>
                </div>
                <div class="product">
                  <div class="product__imgwrapper">
                    <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
                  </div>
                  <div class="product__title">
                    <div class="product__name">Michael Kors</div>
                    <div class="product__price">2 500 р.</div>
                  </div>
                  <div class="product__subtitle">
                    <div class="product__likeImgWrapper">
                      <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
                    </div>
                    <button class="product__btn">В КОРЗИНУ</button>
                  </div>
                </div>
              </div>
              <button class="categoryBlockProducts__seeMore">
                показать еще
              </button>
            </div>
          </div>
          <aside class="shopSection__filters asideFilters">
            <div class="asideFilters__title">ФИЛЬТР ТОВАРОВ</div>
            <div class="asideFilters__priceFilter">
              <div class="asideFilters__heading">Цена</div>
            </div>
            <div class="asideFilters__selectBlock">
              <div class="asideFilters__heading">Материал</div>
              <select name="choice">
                <option value="first">First Value</option>
                <option value="second" selected>Second Value</option>
                <option value="third">Third Value</option>
              </select>
            </div>
            <div class="asideFilters__selectBlock">
              <div class="asideFilters__heading">Цвет</div>
              <select name="choice">
                <option value="first">First Value</option>
                <option value="second" selected>Second Value</option>
                <option value="third">Third Value</option>
              </select>
            </div>
            <div class="asideFilters__buttons filterButtonsBlock">
              <button class="filterButtonsBlock__btn btn--red">Показать</button>
              <button class="filterButtonsBlock__btn">Сбросить</button>
            </div>
          </aside>
        </div>
      </div>
    </section>
    <section class="bestsellers">
      <div class="container">
        <div class="bestsellers__titleWrapper promo__titleWrapper">
          <h2 class="bestsellers__title promo__title">хиты <br> продаж</h2>
        </div>
        <div class="bestsellers__slider bestsellersSlider">
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
          <div class="bestsellersSlider__item product">
            <div class="product__imgwrapper">
              <img class="product__img" src="<?php echo get_template_directory_uri() ?>/assets/img/content/products/product-1.jpg" alt="" >
            </div>
            <div class="product__title">
              <div class="product__name">Michael Kors</div>
              <div class="product__price">2 500 р.</div>
            </div>
            <div class="product__subtitle">
              <div class="product__likeImgWrapper">
                <img class="product__likeImg" src="<?php echo get_template_directory_uri() ?>/assets/img/icons/icon-like2.svg" alt="" >
              </div>
              <button class="product__btn">В КОРЗИНУ</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>


<?php
// get_sidebar();
get_footer();
