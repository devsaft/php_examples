<?php
/*
Template Name: Shipping Page Template
*/
?>

<?php
get_header();
?>

<main class="main">
    <section class="shippingSection">
      <div class="container">
        <h2 class="shippingSection__title section__title">
          <span class="redText">ДОСТАВКА И ОПЛАТА </span> <br>
          ПО МОСКВЕ И РОССИИ
        </h2>
        <div class="shippingSection__wrapper">
          <div class="shippingSection__block shippingSectionBlock">
            <h4 class="shippingSection__heading">Доставка</h4>
            <div class="shippingSection__text">
              <p>Доставка по Москве осуществляется <strong>БЕСПЛАТНО</strong> в день заказа либо в другой удобный для Вас день в указанное Вами время и место.</p>
              <p>Доставка по России осуществляется после оформления заказа посредством почты России, CДЭК или boxberry на Ваш выбор.</p>
              <p style="text-transform: uppercase;">Наши операторы всегда на связи</p>
            </div>
          </div>
          <div class="shippingSection__block shippingSectionBlock">
            <h4 class="shippingSection__heading">оплата</h4>
            <div class="shippingSection__text">Оплата курьеру наличными или банковской картой</div>
            <div class="shippingSectionBlock__imgBlock">
              <div class="shippingSectionBlock__imgWrapper">
                <img class="shippingSectionBlock__img" src="<?php echo get_template_directory_uri() ?>/assets/img/credit_card.png" alt="" >
                <div class="shippingSectionBlock__imgTitle">Visa/Mastercard/МИР</div>
              </div>
              <div class="shippingSectionBlock__imgWrapper">
                <img class="shippingSectionBlock__img" src="<?php echo get_template_directory_uri() ?>/assets/img/courier.png" alt="" >
                <div class="shippingSectionBlock__imgTitle">Курьеру при получении</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>





<?php
// get_sidebar();
get_footer();
